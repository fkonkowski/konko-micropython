# Climbing Tracker

![Event Countdown Device](images/konko-watch.jpeg)
![Event Countdown Device](images/konko-parts.jpeg)

## Introduction
Welcome to the Climbing Tracker project! This project leverages the power of the ESP32 microcontroller and RFID technology to track climbing activities. The core concept revolves around a wearable RFID tag that climbers use during their activities, and an RFID reader integrated into the ESP32 to record their climbing data. The microcontroller then logs and processes this data, providing valuable insights into each climber's activity.

## Features

1. **Real-time Climbing Tracking**: Accurately monitor and log each climbing session as the climber ascends and descends.
2. **Activity Logs**: Record detailed climbing activities, including climbing duration, number of climbs, and more.
3. **Robust and Durable Design**: The wearable is built to endure the rigors of climbing, ensuring reliable and consistent data logging.
4. **User-Friendly Interface**: Simple, intuitive setup process to register climbers' RFID tags and begin tracking.

## Hardware Required

1. ESP32 Microcontroller
2. RFID RC522 Reader
3. RFID Tags/Wristbands
4. Cables, Resistors, and other necessary electronics

## Software Required

1. Arduino IDE
2. ESP32 RFID Library
3. Any necessary drivers for your ESP32 and RFID RC522

## Installation and Setup

1. **Install Arduino IDE**: Download and install the Arduino IDE from the official website.
2. **Install ESP32 on Arduino IDE**: Add the ESP32 board to your Arduino IDE.
3. **Install RFID library**: Download and install the appropriate RFID library to work with the RFID RC522 module.
4. **Connect the Hardware**: Connect the RFID RC522 module with the ESP32 following the wiring diagram in the 'wiring_diagram' directory.
5. **Load the Code**: Load the provided code onto your ESP32.
6. **Register RFID Tags**: Register your RFID tags using the provided registration tool within the code.

## Usage

Once the setup is complete, climbers simply wear their registered RFID tags. The ESP32 with the attached RFID reader will monitor and log climbing activities whenever the RFID tag comes within its reading range. This data can then be retrieved and analyzed for insights into climbing habits, performance, and improvements.

## Contributing

Contributions to the Climbing Tracker project are always welcome. To contribute, please fork the repository and create a pull request for your changes. For major changes, please open an issue first to discuss your proposed changes.

## License

This project is licensed under the MIT License. Please see the `LICENSE` file for more details.

## Contact

If you have any questions, comments, or issues, please don't hesitate to contact us.