import esp32
import utime
import ujson
from machine import UART, Pin, SPI, deepsleep
import tinypico as TinyPICO
import time, micropython
import os
from BLE_Konko import initBLE
import saveDataFile
from synchTime_Konko import synch
import dotstar_Konko
from mfrc522 import MFRC522

os.listdir()
uart1 = UART(1, baudrate=9600, tx=23, rx=25)
wake1 = Pin(15, Pin.IN)

def setRFIDPower(powerState):
    print("setRFIDPower ", powerState)
    Pin(5, Pin.OUT, None)
    Pin(5).value(powerState)
    time.sleep(0.5)
    Pin(5, Pin.OUT, Pin.PULL_HOLD)
    time.sleep(0.035)

setRFIDPower(True)

buzzPin = Pin(26, mode=Pin.OUT)
def buzzer():
    buzzPin(True)
    time.sleep_ms(400)
    buzzPin(False)

timePeriodForSavingMode = 1000 * 180
savingModeTimer = utime.ticks_ms() + timePeriodForSavingMode
esp32.wake_on_ext0(pin=wake1, level=esp32.WAKEUP_ALL_LOW)

def goSleep():
    powerOn = True
    if (powerOn is True and wake1.value() == 0) or savingModeTimer < time.ticks_ms():
        print("Im awake. Going to sleep in 1 seconds")
        dotstar_Konko.checkBattery()
        time.sleep_ms(400)
        dotstar_Konko.checkBattery()
        TinyPICO.set_dotstar_power(False)
        setRFIDPower(False)
        powerOn = False
        deepsleep()

lastSavedTagCode = 1
lastSavedTagTime = time.ticks_ms()

def cb_nfc(code):
    global savingModeTimer
    now = time.ticks_ms()
    buzzer()
    if code > 0 and utime.ticks_diff(savingModeTimer, 1000 * 175) < now:
        saveDataFile.saveData(code, time.localtime())
        savingModeTimer = utime.ticks_add(now, timePeriodForSavingMode)

uart = initBLE()

def on_rx():
    data = ujson.loads(uart.read().decode().strip())
    if data.get("ssid"):
        saveDataFile.saveWiFiSetup(data["ssid"], data["password"])
        print("rx: ", data["ssid"], data["password"])
    if data.get("clearEEPROM"):
        saveDataFile.deleteRFIDFile()
        uart.close()
        uart.is_connected()

dotstar_Konko.checkBattery()
uart.irq(handler=on_rx)
print("pin loaded")
currentTime = utime.localtime()
print("currentTime[0]: ", currentTime[0])
if currentTime[0] == 2000:
    print("synch my time.")
    wifiCredentials = saveDataFile.loadWiFiSetup()
    print(wifiCredentials)
    if wifiCredentials is not None:
        synch(wifiCredentials["ssid"], wifiCredentials["password"])
    else:
        for i in range(0, 15):
            dotstar_Konko.diodOn(255, 0, 0)
            time.sleep_ms(500)
            dotstar_Konko.diodOff()
            time.sleep_ms(500)
    dotstar_Konko.diodOff()
print("start loop 11")
fileOpening = False

sck = Pin(18, Pin.OUT)
mosi = Pin(23, Pin.OUT)
miso = Pin(19, Pin.OUT)
spi = SPI(baudrate=100000, polarity=0, phase=0, sck=sck, mosi=mosi, miso=miso)
sda = Pin(32, Pin.OUT)
powerRFID = Pin(5, Pin.OUT)
powerRFID.value(1)
rdr = MFRC522(spi, sda)
try:
    while True:
        uid = ""
        sda.value(1)
        rdr.init()
        (stat, tag_type) = rdr.request(rdr.REQIDL)
        if stat == rdr.OK:
            (stat, raw_uid) = rdr.anticoll()
            if stat == rdr.OK:
                uid = "0x%02x%02x%02x%02x" % (
                    raw_uid[0],
                    raw_uid[1],
                    raw_uid[2],
                    raw_uid[3],
                )
                cb_nfc(int(uid))
                time.sleep_ms(10)

        goSleep()
        if uart.is_connected() and fileOpening == False:
            print("is connected to ble")
            fileOpening = True
            savingModeTimer = utime.ticks_add(time.ticks_ms(), timePeriodForSavingMode)
            filename = "savedData.txt"
            f = open(filename, "r")
            dataToBesend = []
            for line in f:
                dataToBesend.append(line)
            f.close()
            print("all loaded to variable")
            i = 0
            for activity in dataToBesend:
                if i % 1000 == 0:
                    savingModeTimer = utime.ticks_add(
                        time.ticks_ms(), timePeriodForSavingMode
                    )
                uart.send(str(activity))
                time.sleep_ms(100)
                i += 1
            print("all send")
            fileOpening = False
except KeyboardInterrupt:
    print("Nooo error in RFID")
