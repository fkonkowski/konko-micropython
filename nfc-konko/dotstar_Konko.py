from machine import SPI, Pin
import tinypico as TinyPICO
from dotstar import DotStar
import machine
import time


def diodOn(redIndex = 0, greenIndex = 0, blueIndex = 0, bright = 0.1):

    spi = SPI(sck=Pin(TinyPICO.DOTSTAR_CLK), mosi=Pin(TinyPICO.DOTSTAR_DATA), miso=Pin(TinyPICO.SPI_MISO))
    TinyPICO.set_dotstar_power(True)
    dotstar = DotStar(spi, 1, brightness = bright)
    dotstar[0] = (redIndex, greenIndex, blueIndex)
    dotstar.show()
    return dotstar


def diodOff():
    TinyPICO.set_dotstar_power(False)


def blinkDelay(r,g,b):
    diodOff()
    diodOn(r, g, b)
    time.sleep_ms(500)
    diodOff()

def checkBattery():
    if TinyPICO.get_battery_voltage() >= 3.6:
        diodOn(0,255,0)
    elif TinyPICO.get_battery_voltage() < 3.6 and TinyPICO.get_battery_voltage() >= 3.5:
        diodOn(170,255,43)
    elif TinyPICO.get_battery_voltage() < 3.5 and TinyPICO.get_battery_voltage() >= 3.3:
        diodOn(251, 255, 43)
    elif TinyPICO.get_battery_voltage() < 3.3 and TinyPICO.get_battery_voltage() >= 3.1:
        diodOn(255, 153, 0)
    elif TinyPICO.get_battery_voltage() < 3.1:
        blinkDelay(255, 0, 0)
        blinkDelay(255, 0, 0)
        blinkDelay(255, 0, 0)
        diodOff()
        TinyPICO.set_dotstar_power(False)
        machine.deepsleep()
