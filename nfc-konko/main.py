import esp32
import utime
import ujson
from machine import UART, Pin, SPI, deepsleep
import tinypico as TinyPICO
import time, micropython
import os
from BLE_Konko import initBLE
import saveDataFile
from synchTime_Konko import synch
import dotstar_Konko

os.listdir()
uart1 = UART(1, baudrate=9600, tx=23, rx=25)
wake1 = Pin(15, Pin.IN)

buzzPin = Pin(26, mode=Pin.OUT)
def buzzer():
    buzzPin(True)
    time.sleep_ms(400)
    buzzPin(False)

timePeriodForSavingMode = 1000 * 180
savingModeTimer = utime.ticks_ms() + timePeriodForSavingMode
esp32.wake_on_ext0(pin=wake1, level=esp32.WAKEUP_ALL_LOW)

def goSleep():
    powerOn = True
    if (powerOn is True and wake1.value() == 0) or savingModeTimer < time.ticks_ms():
        print("Im awake. Going to sleep in 1 seconds")
        dotstar_Konko.diodOff()
        time.sleep_ms(400)
        dotstar_Konko.checkBattery()
        TinyPICO.set_dotstar_power(False)
        time.sleep_ms(100)
        setRFIDPower(False)
        powerOn = False

        deepsleep()

lastSavedTagCode = 1
lastSavedTagTime = time.ticks_ms()

def cb_nfc(code):
    global savingModeTimer
    now = time.ticks_ms()
    buzzer()
    print('code',code)
    if code > 0 and utime.ticks_diff(savingModeTimer, 1000 * 175) < now:
        saveDataFile.saveData(code, time.localtime())
        savingModeTimer = utime.ticks_add(now, timePeriodForSavingMode)

uart = initBLE()

def on_rx():
    data = ujson.loads(uart.read().decode().strip())
    if data.get("ssid"):
        saveDataFile.saveWiFiSetup(data["ssid"], data["password"])
        print("rx: ", data["ssid"], data["password"])
    if data.get("clearEEPROM"):
        saveDataFile.deleteRFIDFile()
        uart.close()
        uart.is_connected()

uart.irq(handler=on_rx)
currentTime = utime.localtime()
if currentTime[0] == 2000:
    print("synch my time.")
    wifiCredentials = saveDataFile.loadWiFiSetup()
    print(wifiCredentials)
    if wifiCredentials is not None:
        synch(wifiCredentials["ssid"], wifiCredentials["password"])
    else:
        for i in range(0, 15):
            dotstar_Konko.diodOn(255, 0, 0)
            time.sleep_ms(500)
            dotstar_Konko.diodOff()
            time.sleep_ms(500)
    dotstar_Konko.diodOff()
print("start loop 14")
fileOpening = False
dotstar_Konko.checkBattery()
import NFC_PN532 as nfc

# FUNCTION TO READ
def read_nfc(dev, tmot):
    """Accepts a device and a timeout in millisecs """
    uid = dev.read_passive_target(timeout=tmot)
    if uid is not None:
        uid_ID = "0x%02x%02x%02x%02x" % (
            uid[0],
            uid[1],
            uid[2],
            uid[3],
        )
        cb_nfc(int(uid_ID))

sck = Pin(18, Pin.OUT)
mosi = Pin(23, Pin.OUT)
miso = Pin(19, Pin.OUT)
spi = SPI(baudrate=100000, polarity=0, phase=0, sck=sck, mosi=mosi, miso=miso)
cs = Pin(21, Pin.OUT)
cs.on()

pn532 = nfc.PN532(spi,cs)

# Configure PN532 to communicate with MiFare cards
def setRFIDPower(powerState):
    print("setRFIDPower ", powerState)
    if powerState == True:
        led_pin = Pin(22, Pin.OUT)
        led_pin.value(0)
        time.sleep(0.5)
        led_pin.value(1)
        time.sleep(0.5)
        print('try to reset RFID by on/off')
    else:
        print('RFID OFF NOW power_down')
        status = pn532.power_down()
        print('status', status)

setRFIDPower(True)
time.sleep(0.5)
pn532.SAM_configuration()

ic, ver, rev, support = pn532.get_firmware_version()
print('Found PN532 with firmware version: {0}.{1}'.format(ver, rev))

try:
    while True:
        read_nfc(pn532, 500)
        goSleep()
        if uart.is_connected() and fileOpening == False:
            fileOpening = True
            savingModeTimer = utime.ticks_add(time.ticks_ms(), timePeriodForSavingMode)
            filename = "savedData.txt"
            f = open(filename, "r")
            dataToBesend = []
            for line in f:
                dataToBesend.append(line)
            f.close()
            i = 0
            for activity in dataToBesend:
                if i % 1000 == 0:
                    savingModeTimer = utime.ticks_add(
                        time.ticks_ms(), timePeriodForSavingMode
                    )
                uart.send(str(activity))
                time.sleep_ms(100)
                i += 1
            fileOpening = False
except KeyboardInterrupt:
    print("Nooo error in RFID")
