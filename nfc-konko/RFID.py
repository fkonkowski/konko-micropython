from machine import UART
from machine import Pin
import struct
uart1 = UART(1, baudrate=9600, tx=4, rx=25)
powerRFID = Pin(27, Pin.OUT)
powerRFID.value(1)

# callback to run on detection
def cb(code, facility, card):
    if code > 0 and facility > 0 and card > 0:
        print('Code: {}, Facility: {}, Card: {}'.format(code, facility, card))

# poll the uart
def uart_demo(callback):
	buf = bytearray(4)
	while True:
		if uart1.any():
			uart1.readinto(buf)
			code = struct.unpack('>i',buf)[0]
			facility = code >> 16
			card = code & 0xFFFF
			callback(code, facility, card)

# run the demo
uart_demo(cb)


