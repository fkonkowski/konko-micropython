from time import sleep_ms
from machine import Pin, SPI
from mfrc522 import MFRC522

sck = Pin(18, Pin.OUT)
mosi = Pin(23, Pin.OUT)
miso = Pin(19, Pin.OUT)
spi = SPI(baudrate=100000, polarity=0, phase=0, sck=sck, mosi=mosi, miso=miso)

sda = Pin(21, Pin.OUT)
powerRFID = Pin(5, Pin.OUT)
powerRFID.value(1)

# callback to run on detection
def cb(card):
    if card > 0:
        print('Card: {}'.format( card))


def do_read(callback):
    try:
        rdr = MFRC522(spi, sda)
        uid = ""
        (stat, tag_type) = rdr.request(rdr.REQIDL)
        if stat == rdr.OK:
            (stat, raw_uid) = rdr.anticoll()
            if stat == rdr.OK:
                uid = "0x%02x%02x%02x%02x" % (
                    raw_uid[0],
                    raw_uid[1],
                    raw_uid[2],
                    raw_uid[3],
                )
                callback(int(uid))
                sleep_ms(20)
    except KeyboardInterrupt:
        print("Nooo error in RFID")

