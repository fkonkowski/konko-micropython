import machine
import esp32
from machine import Pin
from time import sleep
import tinypico as TinyPICO

def goSleep():
    powerOn = True
    wake1 = Pin(15, mode=Pin.IN)
    esp32.wake_on_ext0(pin=wake1, level=esp32.WAKEUP_ALL_LOW)

    if powerOn == True and wake1.value() == 0:
        TinyPICO.set_dotstar_power(False)
        print("Im awake. Going to sleep in 2 seconds")
        sleep(2)
        print("Going to sleep now")
        powerOn = False
        machine.deepsleep()
        print("never exe")


while True:
    goSleep()
