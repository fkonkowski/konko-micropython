import esp32
from machine import Pin, deepsleep
from time import sleep
import tinypico as TinyPICO

def goSleep():
    powerOn = True
    esp32.wake_on_ext0(pin=wake1, level=esp32.WAKEUP_ALL_LOW)
    if (powerOn is True and wake1.value() == 0) or savingModeTimer < time.ticks_ms():
        print("Im awake. Going to sleep in 1 seconds")
        dotstar_Konko.checkBattery()
        time.sleep_ms(400)
        dotstar_Konko.checkBattery()
        TinyPICO.set_dotstar_power(False)
        setRFIDPower(False)
        print("wake1", wake1)
        print("Going to sleep now")
        powerOn = False
        deepsleep()
        print("never exe")

