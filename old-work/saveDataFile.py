import os
import ujson


def loadData():
    filename = "savedData.txt"
    with open(filename, "a") as f:
        for line in f:
            print(line)
    f.close()


def saveData(rfId, dataTime):
    filename = "savedData.txt"
    jsonData = {}
    jsonData[rfId] = dataTime
    result = ujson.dumps(jsonData)
    if filename in os.listdir():
        with open(filename, "a") as f:
            f.write(result + "\n")
    else:
        f = open(filename, "w")
        f.write(result + " \n")

    f.close()

def loadWiFiSetup():
    filename = "savedWiFiSetup.json"
    if filename in os.listdir():
        with open(filename) as fp:
            data = ujson.loads(fp.read())
        fp.close()
        print(data)
        return data

def saveWiFiSetup(ssid, password):
    jsonData = {}
    jsonData["ssid"] = ssid
    jsonData["password"] = password
    result = ujson.dumps(jsonData)
    f = open("savedWiFiSetup.json", "w")
    f.write(result)
    f.close()
