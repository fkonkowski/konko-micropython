

def synch(ssid, password):
    import utime
    import network
    import ntptime
    import time
    import dotstar_Konko
    wlan = network.WLAN(network.STA_IF)
    if not wlan.active() or not wlan.isconnected():
        wlan.active(True)
        print("connecting to:", ssid)
        wlan.connect(ssid, password)
        last_blink = utime.ticks_ms()
        led_state = False
        while not wlan.isconnected():
            now = utime.ticks_ms()
            if utime.ticks_diff(now, last_blink) > 500:
                led_state = not led_state
                if led_state:
                    dotstar_Konko.diodOn(0,0,255)
                else:
                    dotstar_Konko.diodOff()
                last_blink = now
            pass

    print("Local time before synchronization：%s" % str(time.localtime()))
    ntptime.settime()
    dotstar_Konko.diodOff()
    print("Local time after synchronization：%s" % str(time.localtime()))
    print("network config:", wlan.ifconfig())
